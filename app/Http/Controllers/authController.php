<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function register(){
        return view('register.form'); //register itu nama folder di view dan form nama file blade nya
    }

    public function welcome(Request $request){
        $fname = $request['fname']; //memasukkan nama valiable 'fname' di form ke variable penampung $fname
        $lname = $request['lname']; //memasukkan nama valiable 'lname' di form ke variable penampung $lname
        return view('register.welcome', compact('fname','lname')); //fname dan lname disini adalah nama variable penampung $fname dan $lname tanpa tanda dolar ($)
    }
}
